import java.util.Scanner;

public class HappyBirthday {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите сегодняшнее число");
        String today = reader.nextLine();
        System.out.println("Введите дату своего рождения");
        String birthday = reader.nextLine();
        String[] ofToday = today.split("[.]"); //Создаём массив.
        String[] ofBirth = birthday.split("[.]"); //Создаём массив.
        int yearOfToday = Integer.parseInt(ofToday[2]); //Приводим к типу integer для дальнейшего сравнения.
        int yearOfBirth = Integer.parseInt(ofBirth[2]);
        int monthOfToday = Integer.parseInt(ofToday[1]);
        int monthOfBirth = Integer.parseInt(ofBirth[1]);
        int dayOfToday = Integer.parseInt(ofToday[0]);
        int dayOfBirth = Integer.parseInt(ofBirth[0]);
        if ((yearOfToday < yearOfBirth) || ((yearOfToday - yearOfBirth) > 120)) {
            System.out.println("Дата введена некорректно");
        } else {
            if (monthOfToday == monthOfBirth) {
                if (dayOfToday == dayOfBirth) {
                    System.out.println("С днем рождения!!!");
                }
            } else if (monthOfToday < monthOfBirth) {
                System.out.println("С наступающим днем рождения!");
            } else {
                System.out.println("С прошедшим днем рождения!");
            }
        }
    }
}

